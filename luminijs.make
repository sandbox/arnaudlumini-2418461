core = 7.x
api = 2

libraries[luminijs][download][type] = 'git'
libraries[luminijs][download][branch] = '1.x'
libraries[luminijs][download][url] = 'git@gitlab.dev.lumini.fr:root/luminijs.git'
libraries[luminijs][destination] = 'libraries'
libraries[luminijs][type] = 'library'
